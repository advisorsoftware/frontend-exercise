# ASI Frontend Coding Exercise

Design and develop a working prototype for a sample Risk Questionnaire application. App's goal is to display given set of questions to users, record their answers & finally produce a numeric score based on user�s answer to each question. Use �rtq.json� (attached in "source") as questionnaire data. 
If you are familiar with the React ecosystem it is expected that this exercise may take you around 8-12 hrs to design, develop, and test.

## General Requirements

1. Core application needs to be built with React (other related libraries are fine to include)
1. Create a branch with your name and a pull request for review by the submission deadline
1. Should be built as a single page javascript application (i.e. uses client-side navigation)
1. Company Name, colors, fonts, and background should be easily configurable so that this app could be reused for many different customers. 
1. Bonus: ES6+ is preferred but not required
1. Bonus: The user should be able to save their work and come back later if they don�t finish
1. Bonus: Show off your styling skills and UI polishing abilities, e.g. tasteful animations between screens, styling that looks great on all devices. 
1. Bonus: App is responsive and works cross browser and different viewport sizes. (Not necessary to be device perfect but basic responsiveness is a bonus)
## User Interface Requirements

### Screens:
1. Intro/Splash Page
1. Demographic and Personal Info Screen
1. 10 Risk Questions (one screen per question). Use the data in rtq.json (available in "source")
1. Summary Screen

### Navigation
1. The top of the application should include the Investment company name or logo. 
1. There should be a progress bar at the top of the application showing the user where they are in the process
1. Forward and backwards buttons for each screen except the summary which will have a "submit" button

### Splash Page
![Splash](https://bitbucket.org/advisorsoftware/frontend-exercise/raw/1266ab2393c77f18304bfdb01e00da0dd4d55a4f/wireframes/splash.png)

### Demographic and Personal Info Screen
Collect the following personal information: email, first name, last name, address, income, age
![Personal](https://bitbucket.org/advisorsoftware/frontend-exercise/raw/1266ab2393c77f18304bfdb01e00da0dd4d55a4f/wireframes/personal.png)

### Risk Question Screen
![Question](https://bytebucket.org/advisorsoftware/frontend-exercise/raw/298f6826122cd7282055186950ad3c1507a278e1/wireframes/question.png)

### Summary Screen
![Summary](https://bitbucket.org/advisorsoftware/frontend-exercise/raw/1266ab2393c77f18304bfdb01e00da0dd4d55a4f/wireframes/summary.png)


